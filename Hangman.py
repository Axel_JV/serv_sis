import random
import os

def run():
    
    my_list = []
    the_word = []
    the_write = []
    aux = 0
    fail = 0
    life = 6
    attemps = range(6)

    with open("./archivos/data.txt", "r", encoding="utf-8") as f:
    
        for line in f:
            my_list.append(line)
        
    rand = random.choice(my_list)
   
    for a in rand:
        the_word.append(a)

    the_word.pop(len(rand) -1)

    for i in the_word:
        the_write.append(" _")
 
    while the_word != the_write and life > 0:
        print("¡Adivina la palabra!\n")
        print(' '.join(the_write))
        letter = input("\n\n Ingresa una letra: ")
        
        for i in the_word:
            if letter == i:
                the_write[aux]=letter
                aux += 1  
            else:
                fail += 1
                aux += 1
        if fail == (len(rand) -1):
            life -= 1
            
        aux = 0 
        fail = 0
        os.system("cls")

    if the_word == the_write:
        print("Felicidades has encontrado la palabra secreta")
    else:
        print("Intentanlo nuevamente, mejor suerte para la proxima")


if __name__ == "__main__":
    run()